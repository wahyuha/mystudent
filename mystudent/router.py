from student.api.viewsets import UserViewSet, GroupViewSet, UniversityViewSet, FeedbackViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'groups', GroupViewSet)
router.register(r'university', UniversityViewSet, base_name='university')
router.register(r'feedback', FeedbackViewSet, base_name='feedback')