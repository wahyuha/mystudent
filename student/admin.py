from django.contrib import admin
from .models import UniversityVendor, University, Facility, ImageUniversity, FacilitiesGroup, FacultiesGroup, Scholarships, Country, Feedback, Degree, Faculty
from django import forms

class AdminStudent(admin.AdminSite):
  site_header = 'Student Management'

class ImageUniversityInline(admin.StackedInline):
  model = ImageUniversity
  extra = 0

class FacilitiesInline(admin.StackedInline):
  model = FacilitiesGroup
  extra = 0

class FacultiesInline(admin.StackedInline):
  model = FacultiesGroup
  extra = 0

class ScholarshipsInline(admin.StackedInline):
  model = Scholarships
  extra = 0

class FeedbackInline(admin.StackedInline):
  model = Feedback
  extra = 0

class FacultyAdmin(admin.ModelAdmin):
  list_display = ('name', 'parent')
  # list_display_links = ('name', 'parent')
  list_per_page = 25
  fields = ('name', 'parent')

class UniversityAdmin(admin.ModelAdmin):
  list_display = ('name', 'expense', 'website', 'city', 'province')
  list_display_links = ('name', 'website')
  search_fields = ('vendor', 'name', 'alias', 'expense', 'short_description', 'province', 'sub_district', 'city')
  list_per_page = 25
  fields = ['vendor', 'name', 'alias', 'accredited', 'expense', 'short_description', 'website', 'email', 'phone', 'phone_2', 'description', 'announcement', 'coordinates', 'address', 'sub_district', 'city', 'province', 'country']

  inlines = [ImageUniversityInline, FacilitiesInline, FacultiesInline, ScholarshipsInline, FeedbackInline]

  def save_model(self, request, obj, forms, change):
    super(UniversityAdmin,self).save_model(request, obj, forms, change)
    # obj.save()

    for afile in request.FILES.getlist('photos_multiple'):
      obj.images.create(file=afile)


admin.site = AdminStudent(name='student')
admin.site.register(UniversityVendor)
admin.site.register(University, UniversityAdmin)
admin.site.register(Facility)
admin.site.register(Country)
admin.site.register(Degree)
# admin.site.register(Category)
admin.site.register(Faculty, FacultyAdmin)