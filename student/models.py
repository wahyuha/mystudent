from django.contrib import admin
from django.db import models
from django.utils.safestring import mark_safe
from django.db.models import Avg

class Facility(models.Model):
  name = models.CharField(max_length=200)

  def __str__(self):
    return self.name

class Degree(models.Model):
  name = models.CharField(max_length=20)
  description = models.CharField(max_length=50, blank=True, null=True)
  semester = models.IntegerField(blank=True, null=True)

  def __str__(self):
    return self.name

# class Category(models.Model):
#   name = models.CharField(max_length=200)

#   def __str__(self):
#     return self.name

class Faculty(models.Model):
  name = models.CharField(max_length=200)
  parent = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True, related_name='faculty_ref')
  # category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)

  def __str__(self):
    return "%s" % (self.name)

class Country(models.Model):
  name = models.CharField(max_length=200)
  code = models.CharField(max_length=10, blank=True, null=True)

  def __str__(self):
    return "%s %s" % (self.name, self.code)

class UniversityVendor(models.Model):
  name = models.CharField(max_length=200)
  has_branch = models.BooleanField(default=False)

  def __str__(self):
    return self.name

class University(models.Model):
  LOW = 'LOW'
  MID = 'MID'
  HIGH = 'HIGH'
  EXPENSE_LEVEL = (
    (LOW, 'Low'),
    (MID, 'Medium'),
    (HIGH, 'High')
  )

  ACCREDITED_LEVEL = (
    ('A', 'A'),
    ('B', 'B'),
    ('C', 'C')
  )
  vendor = models.ForeignKey(UniversityVendor, on_delete=models.CASCADE, null=True)
  name = models.CharField(max_length=200)
  alias = models.CharField(max_length=200, blank=True, null=True)
  short_description = models.CharField(max_length=255, blank=True, null=True)
  description = models.TextField(blank=True, null=True)
  announcement = models.TextField(blank=True, null=True)
  expense = models.CharField(max_length=5, choices=EXPENSE_LEVEL, blank=True, null=True)
  accredited = models.CharField(max_length=1, choices=ACCREDITED_LEVEL, blank=True, null=True)
  email = models.EmailField(max_length=200, blank=True, null=True)
  website = models.CharField(max_length=200, blank=True, null=True)
  phone = models.CharField(max_length=20, blank=True, null=True)
  phone_2 = models.CharField(max_length=20, blank=True, null=True)
  coordinates = models.CharField(max_length=40, blank=True, null=True)
  address = models.CharField(max_length=200, blank=True, null=True)
  sub_district = models.CharField(max_length=200, blank=True, null=True)
  city = models.CharField(max_length=200, blank=True, null=True)
  province = models.CharField(max_length=200, blank=True, null=True)
  country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True)
  created_at = models.DateTimeField(auto_now=True)
  facilities = models.ManyToManyField(
    Facility,
    through='FacilitiesGroup',
    through_fields=('university', 'facility'),
  )
  faculties = models.ManyToManyField(
    Faculty,
    through='FacultiesGroup',
    through_fields=('university', 'faculty'),
  )
  
  def __str__(self):
    return self.name

  def average_rating(self):
    qs = Feedback.objects.filter(university_id=self.id).aggregate(average_rating=Avg('rating'))
    return qs['average_rating']

class FacilitiesGroup(models.Model):
  university = models.ForeignKey(University, on_delete=models.CASCADE)
  facility = models.ForeignKey(Facility, on_delete=models.CASCADE)

class FacultiesGroup(models.Model):
  ACCREDITED_LEVEL = (('A', 'A'),('B', 'B'),('C', 'C'))
  DEGREE_LEVEL = (('A', 'A'),('B', 'B'),('C', 'C'))

  university = models.ForeignKey(University, on_delete=models.CASCADE)
  faculty = models.ForeignKey(Faculty, on_delete=models.CASCADE)
  accredited_faculty = models.CharField(max_length=1, choices=ACCREDITED_LEVEL, blank=True, null=True)
  degree = models.ForeignKey(Degree, on_delete=models.CASCADE)

class ImageUniversity(models.Model):
  university = models.ForeignKey(University, on_delete=models.CASCADE, related_name='images')
  file = models.ImageField(upload_to='images/', blank=True, null=True)
  is_thumbnail = models.BooleanField(default=False)
  position = models.PositiveSmallIntegerField(default=0)

  class Meta:
    ordering = ['position']

  def __str__(self):
    return '%s - %s ' % (self.university, self.file)

class Scholarships(models.Model):
  title = models.CharField(max_length=200, blank=True)
  university = models.ForeignKey(University, on_delete=models.CASCADE, related_name='scholarships')
  banner = models.ImageField(upload_to='images/scholarships/', blank=True, null=True)
  description = models.TextField(blank=True, null=True)
  date_start = models.DateTimeField()
  date_end = models.DateTimeField()

  class Meta:
    ordering = ['date_start']

  def __str__(self):
    return '%s - %s ' % (self.university, self.banner)

class Feedback(models.Model):
  university = models.ForeignKey(University, on_delete=models.CASCADE, related_name='feedbacks')
  rating = models.PositiveSmallIntegerField(blank=True, null=True)
  comment = models.TextField(blank=True, null=True)
  created_at = models.DateTimeField(auto_now=True)
  is_parent = models.BooleanField(default=True)
  refer_to = models.IntegerField(blank=True, null=True)

  # class Meta:
  #   ordering = ['-created_at']
    # ordering_fields = '__all__'
