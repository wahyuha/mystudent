# Generated by Django 2.2 on 2019-05-02 16:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0012_auto_20190502_1628'),
    ]

    operations = [
        migrations.AddField(
            model_name='university',
            name='expense',
            field=models.CharField(choices=[('LOW', 'low'), ('MID', 'medium'), ('HIGH', 'high')], default='', max_length=5),
        ),
    ]
