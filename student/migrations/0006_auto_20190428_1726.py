# Generated by Django 2.2 on 2019-04-28 17:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0005_auto_20190428_1722'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='university',
            name='facility_id',
        ),
        migrations.AddField(
            model_name='facility',
            name='university_id',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='facility', to='student.University'),
        ),
    ]
