from django.contrib.auth.models import User, Group
from student.models import University, Feedback
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from django_filters import rest_framework as filters
from rest_framework.filters import SearchFilter, OrderingFilter
from .serializers import UserSerializer, GroupSerializer, UniversitySerializer, FeedbackSerializer

class UserViewSet(viewsets.ModelViewSet):
  queryset = User.objects.all().order_by('-date_joined')
  serializer_class = UserSerializer

class GroupViewSet(viewsets.ModelViewSet):
  queryset = Group.objects.all()
  serializer_class = GroupSerializer

class UniversityFilter(filters.FilterSet):
  class Meta:
    model = University
    fields = ['expense', 'country', 'facilities']

class FeedbackFilter(filters.FilterSet):
  class Meta:
    model = Feedback
    fields = ['university']

class UniversityViewSet(viewsets.ModelViewSet):
  queryset = University.objects.all()
  serializer_class = UniversitySerializer
  filter_class = UniversityFilter
  filter_backends = (filters.DjangoFilterBackend, SearchFilter, OrderingFilter)
  search_fields = ('name', 'alias', 'short_description', 'province', 'city')
  ordering_fields = ('name', 'created_at', 'average_rating')
  
  # example of newest
  # @action(methods=['get'], detail=False)
  # def newest(self, request):
  #   newest = self.get_queryset().order_by('created_at').last()
  #   serializer = self.get_serializer_class()(newest)
  #   return Response(serializer.data)

class FeedbackViewSet(viewsets.ModelViewSet):
  queryset = Feedback.objects.all()
  serializer_class = FeedbackSerializer

  filter_class = FeedbackFilter
  filter_backends = (filters.DjangoFilterBackend, SearchFilter, OrderingFilter)
