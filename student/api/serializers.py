from django.contrib.auth.models import User, Group
from student.models import University, ImageUniversity, Scholarships, Faculty, Feedback, FacultiesGroup

from rest_framework import serializers

class UserSerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model = User
    fields = ('url', 'username', 'email', 'groups')

class GroupSerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model = Group
    fields = ('url', 'name')

class ImageUniversitySerializer(serializers.ModelSerializer):
  class Meta:
    model = ImageUniversity
    fields = ('is_thumbnail', 'file' ,'position')

class FacultiesSerializer(serializers.ModelSerializer):
  class Meta:
    model = Faculty
    fields = ('__all__')

# class FacultiesGroupSerializer(serializers.ModelSerializer):
#   class Meta:
#     model = FacultiesGroup
#     fields = ('__all__')

class ScholarshipsSerializer(serializers.ModelSerializer):
  class Meta:
    model = Scholarships
    fields = ('__all__')

class FeedbackSerializer(serializers.ModelSerializer):
  class Meta:
    model = Feedback
    fields = ('rating', 'comment' ,'is_parent', 'refer_to', 'created_at')

class UniversitySerializer(serializers.ModelSerializer):
  facilities = serializers.StringRelatedField(many=True)
  # faculties = serializers.StringRelatedField(many=True)
  faculties = FacultiesSerializer(many=True)
  # facultiesGroup = FacultiesGroupSerializer(many=True)
  images = ImageUniversitySerializer(many=True, read_only=True)
  scholarships = ScholarshipsSerializer(many=True)
  # feedbacks = FeedbackSerializer(many=True, read_only=True)

  class Meta:
    model = University
    fields = ('id', 'name', 'accredited', 'expense', 'short_description', 'website', 'email', 'phone', 'phone_2', 'description', 'coordinates', 'address', 'sub_district', 'city', 'province', 'country', 'announcement', 'average_rating', 'created_at', 'facilities', 'faculties', 'images', 'scholarships')