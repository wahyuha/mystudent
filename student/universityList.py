from .models import University
from .serializers import UniversitySerializer
from rest_framework import generics

class UniversityList(generics.ListAPIView):
  serializer_class = UniversitySerializer

  def get_queryset(self):
    name = self.kwargs['name']
    return University.objects.filter(university__name=name)
